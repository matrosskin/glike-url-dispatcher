var _ = require('lodash');
var fs = require('fs');

var config = global.config;

var appPath = process.cwd();

var routingPath = config.urlMappingPath || appPath + '/config/urlMapping';
var urlMapping = require(routingPath);

if (typeof config.controllerSuffix === 'undefined') {
	config.controllerSuffix = 'Controller';
};
if (typeof config.actionSuffix === 'undefined') {
	config.actionSuffix = 'Action';
};
config.controllerNameConverter = config.controllerNameConverter || function(controllerName) {
	return controllerName.replace(/([a-z])([A-Z])/g, "$1-$2");
};

// Start to prepare controller
var controllersPath = config.controllersPath || appPath + '/controllers';
var controllers = {};

function readControllers(path) {
	var controllersDirContent = fs.readdirSync(path);
	for (var fIndex in controllersDirContent) {
		var fileName = controllersDirContent[fIndex];
		var i = fileName.lastIndexOf('.');
		if (i < 0) { // TODO: may be it is neaded for additional checking that the file is directory.
			readControllers(path + '/' + fileName);
		} else {  // TODO: may be it is neaded for additional checking that the file is JavaScript.
			var cleanFileName = fileName.substring(0, i);

			var Controller = require(path + '/' + cleanFileName);
			i = cleanFileName.lastIndexOf(config.controllerSuffix);
			if (i < 0 && config.controlerSuffix) continue;
			var rawControllerName = cleanFileName.replace(config.controllerSuffix, '');
			var controllerName = config.controllerNameConverter(rawControllerName);
			controllerName = controllerName.toLowerCase();

			var actions = {};
			var actionNameList = _.methods(Controller.prototype);
			for (var actionIndex in actionNameList) {
				var rawActionName = actionNameList[actionIndex];
				var actionName = rawActionName.replace(config.actionSuffix, '');
				actionName = config.controllerNameConverter(actionName);

				actions[actionName] = rawActionName;
			}
			controllers[controllerName] = {
				controller: Controller,
				actions: actions
			};
		}
	}
}

readControllers(controllersPath);

function injectRender(controller, request, response) {
	controller.render = function(data, options) {
		options = options || {};

		options.status = options.status || 200;
		options.view = options.view || (request.params.controller + '/' + request.params.action);

		response
			.status(options.status)
			.render(options.view, data); 
			// TODO: The function which we could pass as a callback in render method we could use to implement "afterRender" filters.
	}
}

module.exports = function(app) {

	var urlParams;
	for(var urlMask in urlMapping) {
		urlParams = _.clone(urlMapping[urlMask]);

		// TODO: Implement naming of url. But I don't know if it really need.
		// TODO: Also, I think, it is possible to set params from regexp url pattern
		if ('RegExp:' == urlMask.substring(0, 7)) {
			urlMask = new RegExp(urlMask.substring(7));
		}

		(function(urlParams) {

			app.all(urlMask, function(request, response, next) {
				var controllerName = urlParams.controller || request.params.controller;
				if (!controllers[controllerName]) {
					next(new Error('Failed to load controller: ' + controllerName + '.'));
					return;
				}
				var Controller = controllers[controllerName].controller;

				var actionName;
				if (urlParams.REST) {
					actionName = urlParams.REST[request.method];
				} else {
					actionName = urlParams.action || request.params.action;
				}

				var actualActionName = controllers[controllerName].actions[actionName];
				if (!Controller.prototype[actualActionName]) {
					next(new Error('Failed to detect action: ' + actionName + ' in controller: ' + controllerName + '.'));
					return;
				}

				var curThredUrlParams = _.clone(urlParams);
				request.params = _.extend(curThredUrlParams, request.params);

				var controller = new Controller(); // TODO: in this placce we could implement some dependency injection or something like session object.

				injectRender(controller, request, response);
				controller.request = request;
				controller.response = response;

				controller[actualActionName]();
			});
		})(urlParams);
	}
};
